//
//  main.c
//  Fork
//
//  Created by Diego Marafetti on 3/14/16.
//  Copyright (c) 2016 diego. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, const char * argv[]) {
    
    
    printf("Executing before fork.\n");
    
    pid_t pid = fork();
    
    if(pid == -1) {
    
        perror("Error calling fork()");
        exit(EXIT_FAILURE);
    }
    
    
    if(pid == 0) {
    
        printf("I am the child with pid %d from parent %d\n", getpid(), getppid());
        
        //sleep(5);
        
        printf("child end.\n");
        
    
    } else {
    
        printf("I am the parent with pid %d\n", getpid());
        
        // The waitpid() system call suspends execution of the calling process
        // until a child specified by pid argument has changed state.
        // By default, waitpid() waits only for terminated children,
        // but this behavior is modifiable via the options argument, as described below.
        int exitStatus;
        
        printf("waiting for child to finish...\n");
        
        waitpid(pid, &exitStatus, 0);
        
        printf("Retrieved pid: %d, parent end.\n", pid);
        
    }
    
    return 0;
}

